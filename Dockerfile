FROM debian:bullseye

RUN apt-get update && \
    apt-get install -y git \
        autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev \
        libgmp-dev gawk build-essential bison flex texinfo gperf libtool \
        patchutils bc zlib1g-dev libexpat-dev && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /repo && \
    cd /repo && \
    git clone https://github.com/riscv/riscv-gnu-toolchain && \
    cd riscv-gnu-toolchain && \
    ./configure --prefix=/opt/riscv --with-arch=rv32i --with-abi=ilp32 && \
    make && \
    cd / && \
    rm -Rf repo

ENV PATH="${PATH}:/opt/riscv/bin"
