#!/bin/bash -e

. ./contents.sh

docker build -t docker-registry-auth.k8s.flux.utah.edu/${IMAGE_NAME} .
docker push docker-registry-auth.k8s.flux.utah.edu/${IMAGE_NAME}
docker tag docker-registry-auth.k8s.flux.utah.edu/${IMAGE_NAME} docker-registry.k8s.flux.utah.edu/${IMAGE_NAME}
