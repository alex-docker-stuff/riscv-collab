#!/bin/bash -e

. ./contents.sh

if [ ! -d "home" ]; then
    mkdir home
fi

docker run --rm -it \
    --mount type=bind,src=`readlink -f ./home`,dst=/root/home \
    docker-registry.k8s.flux.utah.edu/${IMAGE_NAME}
